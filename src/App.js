import React from 'react';
import './styles/styles.scss';

function App(){
    return (
        <div>
            <h1 className="mfe2">I am from Micro Frontend 2</h1>
        </div>
    );
}

export default App;
